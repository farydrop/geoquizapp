package com.bignerdranch.android.geomain

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

private const val  EXTRA_ANSWER_IS_TRUE = "com.bignerdranch.android.geomain.answer_is_true"
const val EXTRA_ANSWER_SHOWN = "com.bignerdranch.android.geomain.answer_shown"


class CheatActivity : AppCompatActivity() {

    private lateinit var answerTextView: TextView
    private lateinit var showAnswerButton: Button
    private lateinit var apiLevelText: TextView
    private var answerIsTrue = false


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheat)

        apiLevelText = findViewById(R.id.api_level_text_view)
        val api = Build.VERSION.SDK_INT
        apiLevelText.text = "API Level $api"

        answerIsTrue = intent.getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false)  //чтение значения из дополнения (extra)
        answerTextView = findViewById(R.id.answer_text_view)
        showAnswerButton = findViewById(R.id.show_answer_button)
        showAnswerButton.setOnClickListener {
            val answerText = when {
                answerIsTrue -> R.string.true_button
                else -> R.string.false_button
            }
            answerTextView.setText(answerText)
            setAnswerShowResult(true)
            
        }
    }

    private fun setAnswerShowResult(isAnswerShow: Boolean) {
        val data = Intent().apply {
            putExtra(EXTRA_ANSWER_SHOWN,isAnswerShow)
        }
        setResult(Activity.RESULT_OK,data)
    }

    companion object {
        fun newIntent(packageContext: Context, answerIsTrue: Boolean): Intent{
            return Intent(packageContext, CheatActivity::class.java).apply {
                putExtra(EXTRA_ANSWER_IS_TRUE,answerIsTrue)
            }
        }
    }

}