package com.bignerdranch.android.geomain

import androidx.annotation.StringRes

data class Question (@StringRes val textResId: Int, val answer: Boolean)
            // класс содержит два вида данных (текст вопроса и правильный ответ)
            // аннотация помогает проверить, что в конструкторе используется правильный строковый идентификатор ресура
            // textResId - Int, потому что переменная содержит идентификатор (всегда Int) строкового ресурса

            // data - для всех классов моделей, предназначен для хранения данных. Автоматически определяет
            // такие функции, как equals(), hashCode(), выводит красивое форматирование через toString()