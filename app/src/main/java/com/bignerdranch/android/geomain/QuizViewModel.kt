package com.bignerdranch.android.geomain

import android.util.Log
import androidx.lifecycle.ViewModel

private const val TAG = "QuizViewModel"

class QuizViewModel: ViewModel() {

    //Используйте сохраненное состояние экземпляра, чтобы сохранить минимальное
    //количество информации, необходимой для воссоздания состояния пользовательского
    //интерфейса (например, номер текущего вопроса). Используйте ViewModel,
    //чтобы кэшировать множество данных, необходимых для восстановления интерфейса после
    //изменения конфигурации. Когда activity воссоздается после смерти
    //процесса, используйте информацию о сохраненном состоянии экземпляра, чтобы
    //настроить ViewModel, как будто ViewModel и activity не были уничтожены.

    var currentIndex = 0
    var isCheater = false

    private val questionBank = listOf(
        Question(R.string.question_australia,true),
        Question(R.string.question_oceans,true),
        Question(R.string.question_mideast,false),
        Question(R.string.question_africa,false),
        Question(R.string.question_americas,true),
        Question(R.string.question_asia,true)
    )

    val currentQuestionAnswer: Boolean
        get() = questionBank[currentIndex].answer

    val currentQuestionText: Int
        get() = questionBank[currentIndex].textResId

    fun moveToText() {
        currentIndex = (currentIndex+1)%questionBank.size
    }

    fun prevText() {
        currentIndex =  (currentIndex-1)%questionBank.size
    }

}