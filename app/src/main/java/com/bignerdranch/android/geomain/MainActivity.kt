package com.bignerdranch.android.geomain

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import java.lang.Exception
import java.util.EnumSet.of

private const val TAG = "MainActivity"
private const val KEY_INDEX = "index"                     // ключ для пары "ключ-значение", которые будут храниться в наборе
private const val REQUEST_CODE_CHEAT = 0

class MainActivity : AppCompatActivity() {                //подкласс, обеспечивающий поддержку старых версий андроид

    private lateinit var trueButton: Button               //lateinit, чтобы указать компилятору что вводится ненулевое значение View перед попыткой использовать содержимое свойства
    private lateinit var falseButton: Button
    private lateinit var nextButton: ImageButton
    private lateinit var questionTextView: TextView
    private lateinit var prevButton:ImageButton
    private lateinit var cheatButton: Button
    private val quizViewModel: QuizViewModel by lazy {                  // lazy опускает применение свойства quizViewModel ак val, а не var.
        ViewModelProvider(this).get(QuizViewModel::class.java)    // Потому что нужно захватить и сохранить QuizViewModel, лишь когда создается экземпляр activity, поэтому quizViewModel получает значение только один раз.
    }


    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {  //функция onCreate(Bundle?) вызывается при создании экземпляра подкласса activity
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate(Bundle?) called")       // включение команды регистрации сообщения в onCreate
        setContentView(R.layout.activity_main)            //заполняет макет и выводит его на экран

        val currentIndex = savedInstanceState?.getInt(KEY_INDEX,0) ?: 0
        quizViewModel.currentIndex = currentIndex         // Если оно существует, назначьте его currentIndex. Если значение с ключом index в наборе не существует или если набор пустой, присвойте значение 0.

        //val provider: ViewModelProvider = ViewModelProvider(this)         создает и возвращает ViewModelProvider, связвнный с activity
        //val quizViewMode = provider.get(QuizViewMode::class.java)         возвращает экземпляр QuizViewModel
        //Log.d(TAG,"Got a QuizViewModel: $quizViewMode")                 ViewModelProvider работает как реестр ViewModel. Когда activity делает первый
                                                                           //запрос QuizViewModel, ViewModelProvider создает и возвращает новый экземпляр
                                                                           //QuizViewModel. Когда activity запрашивает QuizViewModel после изменения конфигурации, экземпляр, который был создан изначально, возвращается. Когда
                                                                           //activity завершается (например, когда пользователь нажимает кнопку «Назад»),
                                                                           //пара ViewModel-Activity удаляется из памяти.

        trueButton = findViewById(R.id.true_button)
        falseButton = findViewById(R.id.false_button)
        nextButton = findViewById(R.id.next_button)
        prevButton = findViewById(R.id.prev_button)
        questionTextView = findViewById(R.id.question_text_view)
        cheatButton = findViewById(R.id.cheat_button)

        cheatButton.setOnClickListener{ view ->
            val answerIsTrue = quizViewModel.currentQuestionAnswer
            val intent = CheatActivity.newIntent(this@MainActivity, answerIsTrue)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val options = ActivityOptions.makeClipRevealAnimation(view,0,0,view.width, view.height)
                startActivityForResult(intent, REQUEST_CODE_CHEAT, options.toBundle())
            } else {
                startActivityForResult(intent, REQUEST_CODE_CHEAT)
            }
        }

        trueButton.setOnClickListener{
            checkAnswer(true)
            trueButton.isEnabled = false
            falseButton.isEnabled = false
        }

        falseButton.setOnClickListener{
            checkAnswer(false)
            falseButton.isEnabled = false
            trueButton.isEnabled = false
        }

        nextButton.setOnClickListener {
            quizViewModel.moveToText()
            updateQuestion()
            trueButton.isEnabled = true
            falseButton.isEnabled = true
        }

        prevButton.setOnClickListener {
            quizViewModel.prevText()
            updateQuestion()
        }

        questionTextView.setOnClickListener {
            quizViewModel.moveToText()
            updateQuestion()
            trueButton.isEnabled = true
            falseButton.isEnabled = true
        }
        updateQuestion()

    }

    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int,
                                  data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == REQUEST_CODE_CHEAT) {
            quizViewModel.isCheater =
                data?.getBooleanExtra(EXTRA_ANSWER_SHOWN, false) ?: false
        }
    }

    override fun onStart() {                  // override - приказывает компилятору проследить за тем, чтобы класс действительно содержал преопределяемую функцию
        super.onStart()
        Log.d(TAG,"onStart() called")
    }
    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume() called")
    }
    override fun onPause() {
        super.onPause()
        Log.d(TAG,"onPause() called")
    }

    override fun onSaveInstanceState(outState: Bundle) {    // ля сохранения в набор дополнительных данных, которые затем могут быть считаны в OnCreate(Bundle?), так можно сохранить значение currentIndex после завершения процесса
        super.onSaveInstanceState(outState)
        Log.i(TAG,"onSaveInstanceState")
        outState.putInt(KEY_INDEX,quizViewModel.currentIndex)
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }

    private fun updateQuestion() {
        val questionTextResId = quizViewModel.currentQuestionText
        questionTextView.setText(questionTextResId)       //задаем текст в вопрос по текущему индексу
    }

    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer = quizViewModel.currentQuestionAnswer
        val messageResId = when {
            quizViewModel.isCheater -> R.string.judgment_toast
            userAnswer == correctAnswer -> R.string.correct_toast
            else -> R.string.incorrect_toast
        }
        Toast.makeText(this,messageResId,Toast.LENGTH_SHORT).show()

    }
}